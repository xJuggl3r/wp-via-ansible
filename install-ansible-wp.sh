#!/usr/bin/env bash

# Usage Example:
# ./install_ansible.sh 'v1.1' path/to/local/ansible/hosts/file
# ./install_ansible.sh ~/ansible.cfg
# Look up https://github.com/ansible/ansible for version tags to use

# Install Git
# The most stable way of getting the latest version of ansible to get it directly from the git repo
echo "[Install Ansible] Install Git"
sudo apt-get install -y git-core unzip

# Install Ansible Dependencies
echo '[Install Ansible] Install dependencies'
sudo apt update
sudo apt upgrade -y
sudo apt install -y software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible

# Install Ansible
echo "[Install Ansible] Go to Home folder"
cd ~
if [[ -f /etc/ansible/ansible.cfg ]]
then
    sudo rm ~/ansible.cfg
fi
sudo cat <<EOL >> ansible.cfg
[OCI]
168.138.144.219
EOL

echo '[Install Ansible] Install Ansible now'
sudo apt install ansible

echo '[Install Ansible] Copy Ansible HOSTS'
sudo mkdir -p /etc/ansible
sudo cp ansible.cfg /etc/ansible/ansible.cfg

echo '[Install Ansible] Update Permissions on hosts file'
# Giving exec permission causes issues when running ansible
sudo chmod 644 /etc/ansible/ansible.cfg

echo '[Install Ansible] Export ANSIBLE_HOST path'
export ANSIBLE_HOSTS=/etc/ansible/ansible.cfg

echo '[Install Ansible] Done!'

echo '[Installing WP + Lamp]'
if [[ -f ansible-playbooks-master.zip ]]
then
    sudo rm ~/ansible-playbooks-master.zip
fi
sudo wget https://objectstorage.sa-saopaulo-1.oraclecloud.com/n/gr6gnqikyfe4/b/bucket/o/wordpress-lamp_ubuntu1804.zip
sudo unzip -o wordpress-lamp_ubuntu1804.zip && sudo rm wordpress-lamp_ubuntu1804.zip
cd wordpress-lamp_ubuntu1804

echo '#########################################################################'
echo 'Edite vars/default.yml e execute: ansible-playbook localhost playbook.yml'
echo '#########################################################################'